;(function() {
"use strict"

function engine(name) {

	var e = {
		name: name,
		internals: {},
		init: function() {

			this.internals = {ports:{},arcs:{},nodes:[],tokens:{}};
		},		
		setPort: function(label,options) {
			/* TODO parse the options object */	
			var ops = (typeof options === "object") ? options : {sticky:false,care:true};
			ops.type = "port";
			this.internals.ports[label] = ops;
			return this;
		},
		setPorts: function() {
			if (arguments.length === 0)
				return this;

			var options = (typeof arguments[ arguments.length - 1 ] === "object") ? arguments[arguments.length - 1] : {sticky:false,care:true};

			for (var i = 0; i < arguments.length; i++) {
				if (typeof arguments[i] === "string") {
					this.setPort(arguments[i],options);
				}
			}

			return this;
		},
		setPortsInput: function() {
			if (arguments.length === 0)
				return this;

			for (var i = 0; i < arguments.length; i++) {
				if (typeof arguments[i] === "string") {
					var port = this.internals.ports[arguments[i]];
					if (port === undefined) 
						continue;

					port.input = true; port.output = false;
				}					
			}		

			return this;
		},
		setPortsOutput: function() {
			if (arguments.length === 0)
				return this;

			for (var i = 0; i < arguments.length; i++) {
				if (typeof arguments[i] === "string") {
					var port = this.internals.ports[arguments[i]];
					if (port === undefined) 
						continue;

					port.input = false; port.output = true;
				}					
			}		

			return this;
		},
		port: function(name) {
			return this.internals.ports[name];
		},
		setArc: function(source,sink) {
			
			/* if source | sink not defined then autocreate first */
			if (this.internals.ports[source] === undefined) 
				this.setPort(source);
				
			if (this.internals.ports[sink] === undefined)
				this.setPort(sink);

			this.internals.arcs[source] = sink;

			return this;
		},
		arc: function(name) {
			return this.internals.arcs[name];
		},
		setNode: function(actor,a,b,c,d) {
			if (typeof actor != "object")
				return this;
	
			this.internals.nodes.push({type:"actor",actor:actor,a:a,b:b,c:c,d:d});
			
			return this;
		},
		renderon: function(canvasel) {

			/* TODO */

			return true;
		},
	};
	return e;
}




window.dataflow = {
	engine: function(name) {
			var e = engine(name);
			e.init();
			return e;
	},
	render: function(engine,canvasid) {
		if (engine === undefined || canvasid === undefined)
			return false;

		var el = document.body.querySelector('#' + canvasid);
		if (el === undefined) 
			return false;

		/* render to the canvas object */		
		return engine.renderon(el);
	},

};

}());
