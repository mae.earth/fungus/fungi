/* eslint-disable */
;(function() {
"use strict"

var Unknown = {
		view: function(v) {
			return m("code.z-unknown",v.attrs.nodeName);
		}
};

var Notemplate = {
	view: function(v) {
		return m("code.z-unknown","template " + v.attrs.name + " not found");
	}
};

var Label = {
	view: function(v) {
		return m("label",{},v.attrs.label);
	}
};



/* templating */


function template(id) {
	if (id.length == 0)
		return null;

	return document.body.querySelector('template[id="' + "widget:" + id + '"]');
}	

function buildFromTemplate(d,t) {
	console.log("building from template ",t,d);

	return buildChildren(d,t.content);
	//return m("span","template " + t.id);
}

function xhr(method) {
	var r = null;
	switch (method) {
		case "put":
		r = function(form) { console.log("xhr put ",form); }
	break;
	case "patch":
		r = function(form) { console.log("xhr patch ",form); }
	break;
	case "get":
		r = function(form) { console.log("xhr get ",form); }
	break;
	case "head":
		r = function(form) { console.log("xhr head ",form); }
	break;
	case "delete":
		r = function(form) { console.log("xhr delete ",form); }
	break;
	default:
		r = function(form) { console.log("xhr post ",form); }
	break;					
	}
	return r;
}

function buildChildren(d,e) {
	console.log("building children ",e,d);

	var all = [];

	_.map(e.children,function(k) {

		var usedtemplate = false;
		var lk = k.nodeName.toLowerCase();

		switch (k.nodeName) {
			case "DIV":
				if (k.dataset.widget != null) {
					var t = template(k.dataset.widget);
					if (t == null) 
						all.push(m(Notemplate,{name:k.dataset.widget}));
					else 
						all.push(buildFromTemplate(k.dataset,t));

					usedtemplate = true;
				} 
			break;
			case "FORM":
				/* check is we are using a restful interface */
				if (k.dataset.xhr === "yes") {
					var r = xhr(k.dataset.xhrMethod);

					all.push(m("form.z-xhr",{method:"post",action:k.action,enctype:k.enctype,onsubmit:function(form) { r(form); return false;}},buildChildren(d,k)));
				} else { 
					all.push(m("form",{method:k.method,action:k.action,enctype:k.enctype},buildChildren(d,k)));				
				}
				usedtemplate = true;
			break
			case "LABEL":

				all.push(m(Label,{data:k.dataset,label:k.innerText}));
			break;
			case "BUTTON":

				all.push(m("button",{type:k.type},k.innerText));					
			break;
			case "INPUT":

				var attrs = {type:k.type,min:k.min,max:k.max,maxlength:k.maxlength,minlength:k.minlength,
									name:k.name,value:k.value,placeholder:k.placeholder};
				if (k.pattern != "")
					attrs.pattern = k.pattern;
				
			
				all.push(m("input",attrs));
				
			break;
			case "SELECT":
				all.push(m("select",{name:k.name,value:k.value},buildChildren(k.dataset,k)));
				usedtemplate = true;
			break;
			case "OPTION":
				all.push(m("option",{value:k.value},k.innerText));
			break;
			case "TEXTAREA":
				all.push(m("textarea",{name:k.name,placeholder:k.placeholder},k.value));
			break;
			default:
				all.push(m(Unknown,{nodeName:k.nodeName}));
			break;
		}
 		
		if (k.children != null && k.children.length > 0 && !usedtemplate)
			all.push(m(lk,buildChildren(k.dataset,k)));
		
	});

	return all;
}


function build(e) {
	console.log("building widget ",e);
	
	var aspect = e;
	if (e.nodeName === "TEMPLATE")
		aspect = e.content;

	var all = [];	

	all.push(buildChildren(null,e));	
	
	console.log(all);
	var w = {
		data:{all:all},
		view:function(v) {

			return [v.state.data.all];
		}
	};
	return w;	
}



window.z = {
	consume: function(e) {
		return build(e);
	},

};

}());
