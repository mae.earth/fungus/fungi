package main

import (
	"strings"
	"sort"
	"reflect"
	"fmt"
	"crypto/sha256"
	"time"
)


/* TODO: keep track of stuff with vector clocks, need an ABAC setup per Attribute, the prefix and name + modifier is useful here
 * then apply to a environment to form a technique for rule 
 */

const (
	PrefixDelimiter = "."
)

type AttributeGroup struct {
	Name string

	Attributes []*Attribute
}

func (ag *AttributeGroup) IsGlobal() bool {
	if ag.Name == "" {
		return true
	}
	return false
}

/* NewAttributeGroup */
func NewAttributeGroup(name string) *AttributeGroup {
	return &AttributeGroup{Name:name,Attributes:make([]*Attribute,0)}
}



/* Name */
func Name(parts ...string) string {
	
	out := make([]string,0)
	for _,p := range parts {
		if p == PrefixDelimiter {
			continue
		}
		out = append(out,strings.Replace(p,PrefixDelimiter,"",-1))
	}

	return strings.Join(out,PrefixDelimiter)
}

/* Attribute */
type Attribute struct {
	Prefix string        `json:"prefix"`/* prefixed in dot notation to describe relationships */
	Name string          `json:"name"`/* actual name of the attribute, full name would be prefix.name */
	Type string          `json:"CT"`/* type of value, string, int, float64, email etc, not required to be go atom */
	Disposition string   `json:"CD"`/* how to display/interact with the type */
	Modifier string      `json:"modifier"`/* used to add modification to the type and value, such as Uppercase */
	Value string        `json:"value"` /* actual value of the attribute, TODO: this could be []byte instead */

	Created time.Time  `json:"created"`
	Modified time.Time `json:"modified"`

	/* transit */
	Id uint64 `json:"-"`
}

/* PrefixedName */
func (a Attribute) PrefixedName() string {
	return strings.Join([]string{a.Prefix,a.Name},PrefixDelimiter)
}

/* Checksum */
func (a Attribute) Checksum() []byte {

	h := sha256.New()
	fmt.Fprintf(h,"%s\n",a.Prefix)
	fmt.Fprintf(h,"%s\n",a.Name)
	fmt.Fprintf(h,"%s\n",a.Type)
	fmt.Fprintf(h,"%s\n",a.Disposition)
	fmt.Fprintf(h,"%s\n",a.Modifier)
	fmt.Fprintf(h,"%s\n",a.Value)

	return h.Sum(nil)
}

/* Clone */
func (a Attribute) Clone(value string) Attribute {
	return Attribute{Prefix:a.Prefix, Name:a.Name, Type: a.Type, Disposition: a.Disposition, 
									 Modifier: a.Modifier, Value: value, Created: a.Created, Modified: a.Modified}
}

/* Copy */
func (a Attribute) Copy() *Attribute {
	return &Attribute{Prefix:a.Prefix, Name:a.Name, Type: a.Type, Modifier: a.Modifier, Value: a.Value,
										Disposition: a.Disposition,Created: a.Created,Modified:a.Modified}
}

func (a Attribute) CopyWithId(id uint64) *Attribute {
	attr := a.Copy()
	attr.Id = id
	return attr
}

/* toAttribute */
func toAttribute(m map[string]string) *Attribute {

	return &Attribute{Prefix: m["attribute.prefix"], 
										 Name: m["attribute.name"], 
										 Type: m["attribute.type"], 
										 Modifier: m["attribute.modifier"],
										 Value: m["attribute.value"],
										 Disposition: m["attribute.disposition"],
										 Created: time.Now(),
										 Modified: time.Now()}
	
}

func toMetaAttribute(m map[string]string) *Attribute {

	return &Attribute{Prefix: m["meta.prefix"],
										 Name: m["meta.name"],
										 Type: m["meta.type"],
										 Modifier: m["meta.modifier"],
										 Value: m["meta.value"],
										 Disposition: m["meta.disposition"],
										 Created: time.Now(),
										 Modified: time.Now()}
}




/* Attributes */
type Attributes []Attribute


/* Checksum */
func (atts Attributes) Checksum() []byte {

	h := sha256.New()
	for _,a := range atts {
		h.Write(a.Checksum())
		h.Write([]byte("\n"))
	}

	return h.Sum(nil)
}

/* Prefixes */
func (atts Attributes) Prefixes() []string {

	p := make(map[string]int,0)

	for _,att := range atts {
		if _,exists := p[att.Prefix]; !exists {
			p[att.Prefix] = 0
		}
		p[att.Prefix] ++
	}

	out := make([]string,len(p))

	i := 0
	for key,_ := range p {
		out[i] = key
		i++
	}

	sort.Slice(out,func(i,j int) bool { return out[i] < out[j] })

	return out
}

/* PrefixedNames */
func (atts Attributes) PrefixedNames() []string {

	out := make([]string,len(atts))
	
	for i := 0; i < len(atts); i++ {
		out[i] = atts[i].PrefixedName()
	}

	return out
}

/* Lookup */
func (atts Attributes) Lookup(name string) *Attribute {

	/* find the first attribute with the name */

	for _,att := range atts {
		if att.Name == name {
			return att.Copy()
		}
	}

	return nil
}

/* PrefixedLookup */
func (atts Attributes) PrefixedLookup(prefix,name string) *Attribute {

	for _,att := range atts {
		if att.Prefix == prefix && att.Name == name {
			return att.Copy()
		}
	}

	return nil
}


/* tools */

/* ToAttributes */
func ToAttributes(parameters ...interface{}) ([]Attribute,error) {
	/* process key=value pairs */
	
	/* check we actually have pairs */
	if len(parameters) % 2 != 0 {
		return nil,fmt.Errorf("expecting even number of parameters")
	}

	/* first check that we have (string)key = value(interface{}) */
	for i := 0; i < len(parameters); i += 2 {
		
		if reflect.TypeOf(parameters[i]).String() != "string" {
			return nil,fmt.Errorf("expecting string at position %d but was %q",i,reflect.TypeOf(parameters[i]).String())
		}
		
	}
	
	if len(parameters) == 0 {
		return nil,nil
	}

	attrs := make([]Attribute,len(parameters) / 2)

	now := time.Now()

	/* now process */
	name := ""
	count := 0
	for i := 0; i < len(parameters); i ++ {

		if i % 2 == 0 {
			
			name = parameters[i].(string)
			continue
		}

		value := ""
		
		/* value */
		switch reflect.TypeOf(parameters[i]).String() {
			case "string":
				
				value = parameters[i].(string)

			break
			case "int":

				value = fmt.Sprintf("%d",parameters[i].(int))
			
			break
			case "float64":

				value = fmt.Sprintf("%f",parameters[i].(float64))

			break
			case "bool":

				if v := parameters[i].(bool); v {
					value = "true"
				} else {
					value = "false"
				}

			break
			default: 
				return nil,fmt.Errorf("unknown type at position %d, %q",i,reflect.TypeOf(parameters[i]).String())	
			break
		}

		prefix := ""
		
		/* break the name in a prefix (if present) */
		if idx := strings.LastIndex(name,PrefixDelimiter); idx != -1 {
			prefix = name[:idx]
			name = name[idx + 1:] /* TODO: possible problem here, buffer overflow */
		}

		attrs[count] = Attribute{Prefix:prefix,
														 Name:name,
														 Type:reflect.TypeOf(parameters[i]).String(),
														 Modifier:"",
														 Disposition: "text/plain",
														 Created: now,
														 Modified: now,
														 Value:value}
		count++
	}


	return attrs,nil
}


type Updater []Attribute

/* Overwrite */
func (u Updater) Overwrite(attr *Attribute) []Attribute {

	if attr == nil {
		return u
	}

	found := false

	for i := 0; i < len(u); i++ {
		if u[i].Prefix == attr.Prefix && u[i].Name == attr.Name {
			u[i] = Attribute{Prefix:attr.Prefix,Name:attr.Name,Type:attr.Type,Disposition:attr.Disposition,
											 Modifier:attr.Modifier,Value:attr.Value,Created:u[i].Created,Modified:attr.Modified}
			found = true
			break
		}
	}

	if !found {
		u = append(u,Attribute{Prefix:attr.Prefix,Name:attr.Name,Type:attr.Type,Disposition:attr.Disposition,
													 Modifier:attr.Modifier,Value:attr.Value,Created: attr.Created, Modified: attr.Modified})
	}

	return u		
}

/* OverwriteValue */
func (u Updater) OverwriteValue(prefix,name string, value interface{}) []Attribute {

	for i := 0; i < len(u); i++ {
		attr := u[i].Copy()
		if u[i].Prefix == attr.Prefix && u[i].Name == attr.Name {
	
			
		_value := ""
		
		/* value */
		switch reflect.TypeOf(value).String() {
			case "string":
				
				_value = value.(string)

			break
			case "int":

				_value = fmt.Sprintf("%d",value.(int))
			
			break
			case "float64":

				_value = fmt.Sprintf("%f",value.(float64))

			break
			case "bool":

				if v := value.(bool); v {
					_value = "true"
				} else {
					_value = "false"
				}

			break
			default: 
				return u
			break
		}
	
			u[i] = Attribute{Prefix:attr.Prefix,Name:attr.Name,Type:reflect.TypeOf(value).String(),Disposition:"text/plain",
											 Modifier:attr.Modifier,Value:_value,Created:u[i].Created,Modified:time.Now()}
			break
		}
	}

	return u
}


func (u Updater) Remove(prefix,name string) []Attribute {

	attrs := make([]Attribute,0)

	for i := 0; i < len(u); i++ {
		if u[i].Prefix == prefix && u[i].Name == name {
			continue
		}
		attrs = append(attrs,u[i])
	}

	return attrs
}














