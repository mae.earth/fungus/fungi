package main

import (
	"testing"
	. "github.com/smartystreets/goconvey/convey"
	
	"encoding/hex"
	"strings"
)

func flatten(list ...interface{}) string {
	out := make([]string,0)
	for _,a := range list {
		if v,ok := a.(string); ok {
			out = append(out,v)
		} else if v,ok := a.([]string); ok {
			out = append(out,v...)
		}
	}
	return strings.Join(out,",")
}
			


func Test_Attribute(t *testing.T) {
	Convey("Attributes",t,func() {
		Convey("Name",func() {
			So(Name("foo","bar"),ShouldEqual,"foo.bar")

			Convey("bad",func() {
				So(Name("foo","bar",PrefixDelimiter),ShouldEqual,"foo.bar")
				So(Name("foo.bar",PrefixDelimiter),ShouldEqual,"foobar")
			})

		})

		Convey("Tools",func() {
			Convey("ToAttributes",func() {
				attrs,err := ToAttributes("one","foo bar","two",2,Name("other","one"),0.3)
				So(err,ShouldBeNil)

				So(attrs,ShouldNotBeNil)
				So(len(attrs),ShouldEqual,3)

				So(attrs[0].Prefix,ShouldBeEmpty)
				So(attrs[0].Name,ShouldEqual,"one")
				So(attrs[0].Type,ShouldEqual,"string")
				So(attrs[0].Value,ShouldEqual,"foo bar")
				So(attrs[0].Modifier,ShouldBeEmpty)

				So(hex.EncodeToString(attrs[0].Checksum()),ShouldEqual,"2c5ed29a2dfec60ceb6ffb277f0b199732049282f23f0d35d9aefb54904f0c17")
				So(hex.EncodeToString(attrs[1].Checksum()),ShouldEqual,"aa22ec9ca321af6d9f9fbae578f9ce55cc683b928e4b6cd2584fd23deb4060a5")
				So(hex.EncodeToString(attrs[2].Checksum()),ShouldEqual,"4d76c2a391744dc8f2acc99902f63d1d45302e4d963b6e9c1aab7daa40aa6503")

				So(attrs[2].Prefix,ShouldEqual,"other")
				So(attrs[2].Name,ShouldEqual,"one")

				So(flatten(Attributes(attrs).PrefixedNames()),ShouldEqual,flatten(".one",".two","other.one"))
				So(hex.EncodeToString(Attributes(attrs).Checksum()),ShouldEqual,"fec2432b5074bce04c8e26acf3fe77bd27c9659c52c47e4f4fc4c98ae69c73ee")

				So(Attributes(attrs).Lookup(""),ShouldBeNil)
				attr := Attributes(attrs).Lookup("one")
				So(attr,ShouldNotBeNil)
				So(hex.EncodeToString(attr.Checksum()),ShouldEqual,"2c5ed29a2dfec60ceb6ffb277f0b199732049282f23f0d35d9aefb54904f0c17")

				So(Attributes(attrs).PrefixedLookup("empty","room"),ShouldBeNil)
				attr = Attributes(attrs).PrefixedLookup("","one")
				So(attr,ShouldNotBeNil)
				So(hex.EncodeToString(attr.Checksum()),ShouldEqual,"2c5ed29a2dfec60ceb6ffb277f0b199732049282f23f0d35d9aefb54904f0c17")

				attr = Attributes(attrs).PrefixedLookup("other","one")
				So(attr,ShouldNotBeNil)
				So(hex.EncodeToString(attrs[2].Checksum()),ShouldEqual,"4d76c2a391744dc8f2acc99902f63d1d45302e4d963b6e9c1aab7daa40aa6503")
			})
		
			Convey("Updater",func() {
				attrs,err := ToAttributes("one","foo bar","two",2,Name("other","one"),0.3)
				So(err,ShouldBeNil)
				So(attrs,ShouldNotBeNil)
				So(len(attrs),ShouldEqual,3)

				So(hex.EncodeToString(attrs[0].Checksum()),ShouldEqual,"2c5ed29a2dfec60ceb6ffb277f0b199732049282f23f0d35d9aefb54904f0c17")
				
				attrs = Updater(attrs).Overwrite(&Attribute{Prefix:"other",Name:"two",Type:"string",Value:"the number two"})
				So(len(attrs),ShouldEqual,4)

				attrs = Updater(attrs).Overwrite(&Attribute{Name:"one",Type:"string",Value:"something else"})
				So(hex.EncodeToString(attrs[0].Checksum()),ShouldNotEqual,"2c5ed29a2dfec60ceb6ffb277f0b199732049282f23f0d35d9aefb54904f0c17")	
				So(hex.EncodeToString(attrs[0].Checksum()),ShouldEqual,"dacfee8ca4b26b7ec367b19fd69f9d573c6e20d8eaf9a91cf85a0dbf1bb00603")

				attrs = Updater(attrs).Remove("","one")
				So(len(attrs),ShouldEqual,3)

			})

				

		})
	})
}
