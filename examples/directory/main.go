package main

import (
	"flag"
	"net/http"
	"log"
	"time"
	"context"
	"strings"
	"html/template"
	"errors"
	"io"
	"io/ioutil"
	"mime"
	"path"
	"strconv"

	"encoding/json"

	
	"github.com/GeertJohan/go.rice"

	bolt "go.etcd.io/bbolt"

)

var (
	ErrBadDatabase = errors.New("Bad Database")
	ErrNotFound = errors.New("Not Found")
	ErrNoAspect = errors.New("No Aspect")
	ErrAlreadyExists = errors.New("Already Exists")
	ErrNoRootAttributes = errors.New("Root Can Not Have Attributes")
)


func main() {

	optHost := flag.String("host","localhost:8088","host to run on")
	optDatabase := flag.String("database","objects.db","database file to write/read from")

	flag.Parse()

	/* make use of rice to embeddied html resources into the binary, 
	 * here we serve those static files from the /static/ url path.
   */	
	box := rice.MustFindBox("static")
	serv := http.StripPrefix("/static/",http.FileServer(box.HTTPBox()))
	http.Handle("/static/", serv)

	serv2 := http.FileServer(box.HTTPBox())
	http.Handle("/favicon.ico", serv2)






	/* open a bolt database; we will store all our attributes objects here */
	db,err := bolt.Open(*optDatabase,0666,&bolt.Options{Timeout: 3 * time.Second})
	if err != nil {
		log.Fatalf("unable loading database %q -- %v\n",*optDatabase,err)
	}

	/* wrap all html interactions with our router */
	r := new(Router)
	r.db = db
	r.get = GetHandler
	r.put = PutHandler
	r.post = PostHandler
	r.del = DeleteHandler	

	http.Handle("/",r)

	
	log.Printf("running on %s\n", *optHost)
	log.Fatal(http.ListenAndServe(*optHost,nil))
}

/* Response */
type Response struct {
	StatusCode int	
	Err error
}

/* Okay */
func Okay() Response {
	return Response{StatusCode: 200 }
}

/* InternalServerError */
func InternalServerError(err error) Response {
	return Response{StatusCode: 500, Err: err}
}

/* NotFound */
func NotFound() Response {
	return Response{StatusCode: 404}
}

func BadRequest() Response {
	return Response{StatusCode: 400}
}

func BadRequestWithError(err error) Response {
	return Response{StatusCode: 400, Err: err}
}

func RedirectToURL() Response {
	return Response{StatusCode: 302}
}

/* T our generalised struct for passing to templates */
type T struct {
	Object *Object

}


/* Render a template */
func Render(ctx context.Context,w http.ResponseWriter,fragment string,t T) Response {

	/* the user can be lax with the spaces */
	fragment = strings.TrimSpace(strings.Replace(fragment," ","_",-1))

	box,err := rice.FindBox("static")
	if err != nil {
		log.Printf("Render error loading rice box -- %v\n",err)
		return InternalServerError(err)
	}
	
	body,err := box.String("templates.html")
	if err != nil {
		log.Printf("Render error loading templates.html from rice box -- %v\n",err)
		return InternalServerError(err)
	}

	m := template.FuncMap{}

	tmpl,err := template.New("~").Funcs(m).Parse(body)
	if err != nil {
		log.Printf("Render error parsing template -- %v\n",err)
		return InternalServerError(err)
	}

	if err := tmpl.ExecuteTemplate(w,fragment,t); err != nil {
		log.Printf("Render error executing fragment %q -- %v\n",fragment,err)
		return InternalServerError(err)
	}

	return Okay()
}



type Func func(context.Context,http.ResponseWriter,*http.Request) Response 

/* Router */
type Router struct {
	get Func
	put Func
	post Func
	del Func

	db *bolt.DB
}

type ContextKey string

func toDatabase(ctx context.Context) *bolt.DB {
	if v := ctx.Value(ContextKey("database")); v != nil {
		db,ok := v.(*bolt.DB)
		if ok {
			return db
		}
	}
	return nil
}


/* ServeHTTP */
func (r *Router) ServeHTTP(w http.ResponseWriter, req *http.Request) {

	ctx := context.WithValue(context.Background(), ContextKey("database"), r.db)

	resp := Response{StatusCode: 404}

	/* Check for the method required */
	switch RequestMethod(ctx,req) {
		case "get":

			if r.get != nil {
				resp = r.get(ctx,w,req)
			}

		break
		case "put":

			if r.put != nil {
				resp = r.put(ctx,w,req)
			}


		break
		case "post":

			if r.post != nil {
				resp = r.post(ctx,w,req)
			}

		break
		case "delete":

			if r.del != nil {
				resp = r.del(ctx,w,req)
			}

		break
	}

	switch resp.StatusCode {
		case 302:
			http.Redirect(w,req,req.URL.Path,302)
		break
		case 404:
			http.NotFound(w,req)
		break
		case 400,401,500,501:
			if resp.Err != nil {
				log.Printf("response error %v (%d)\n",resp.Err,resp.StatusCode)
				http.Error(w,resp.Err.Error(),resp.StatusCode)
			} else {
				http.Error(w,http.StatusText(resp.StatusCode),resp.StatusCode)	
			}
		break
	}
}



/* GetHandler */
func GetHandler(ctx context.Context,w http.ResponseWriter, req *http.Request) Response {
	/* using the path read a bolt bucket/file */
	
	log.Printf("GetHandler %q\n",req.URL.Path)

	d,obk,err := TimedStat(ctx,req.URL.Path)
	
	if err != nil {
		if err == ErrNotFound {
			return NotFound()
		}
		log.Printf("GetHandler error from database -- %v\n",err)
		return InternalServerError(err)
	}

	log.Printf("TimedStat of %q took %s\n",req.URL.Path,d)

	return Render(ctx,w,"sample",T{Object:obk})
}



/* PutHandler */
func PutHandler(ctx context.Context,w http.ResponseWriter, req *http.Request) Response {

	log.Printf("PutHandler %q\n",req.URL.Path)

	
	elements,err := ProcessRequestForm(ctx,req)
	if err != nil {
		return BadRequestWithError(err)
	}

	element := elements.Find("_aspect")
	if element == nil {
		return BadRequestWithError(err)
	}

	aspect := string(element.Content)

	switch aspect {
		case "attribute":
			/* basic to start with */

			m := elements.ToStringMap()

			attr := toAttribute(m)

			id,exists := m["_id"]
			if !exists {
				return BadRequest()
			}
			
			attrname,err := strconv.ParseInt(id,10,64)
			if err != nil {
				return BadRequestWithError(err)
			}
			
			if err := UpdateAttribute(ctx,req.URL.Path,uint64(attrname),attr); err != nil {
				if err == ErrNotFound {
					return NotFound()
				}
				if err == ErrAlreadyExists {
					return BadRequestWithError(err)
				}
				return InternalServerError(err)
			}

		break
		case "meta":

			m := elements.ToStringMap()

			attr := toMetaAttribute(m)

			id,exists := m["_id"]
			if !exists {
				return BadRequest()
			}

			attrname,err := strconv.ParseInt(id,10,64)
			if err != nil {
				return BadRequestWithError(err)
			}

			if err := UpdateMetaAttribute(ctx,req.URL.Path,uint64(attrname),attr); err != nil {
				if err == ErrNotFound {
					return NotFound()
				}
				if err == ErrAlreadyExists {
					return BadRequestWithError(err)
				}
				return InternalServerError(err)
			}		
		break
	}	

	return ResponseOkay(ctx,req)
}


/* PostHandler */
func PostHandler(ctx context.Context,w http.ResponseWriter,req *http.Request) Response {

	log.Printf("PostHandler %q\n",req.URL.Path)


	elements,err := ProcessRequestForm(ctx,req)
	if err != nil {
		return BadRequestWithError(err)
	}

	element := elements.Find("_aspect")
	if element == nil {
		return BadRequestWithError(ErrNoAspect)
	}

	aspect := string(element.Content)

	switch aspect {
		case "object":

			bucketel := elements.Find("object.name")
			bucket := strings.Replace(string(bucketel.Content)," ","-",-1)

			if len(bucket) == 0 {
				bucket = autoname()
			}

			if err := CreateBucket(ctx,req.URL.Path,bucket); err != nil {
				if err == ErrNotFound {
					return NotFound()
				}
				if err == ErrAlreadyExists {
					return BadRequestWithError(err)
				}
				return InternalServerError(err)
			}
		break
		case "attribute":

			/* basic to start with */
			attr := toAttribute(elements.ToStringMap())
			
			if err := CreateAttribute(ctx,req.URL.Path,attr); err != nil {
				if err == ErrNotFound {
					return NotFound()
				}
				if err == ErrAlreadyExists {
					return BadRequestWithError(err)
				}
				return InternalServerError(err)
			}

		break
		case "meta":

			attr := toMetaAttribute(elements.ToStringMap())

			if err := CreateMetaAttribute(ctx,req.URL.Path,attr); err != nil {
				if err == ErrNotFound {
					return NotFound()
				}
				if err == ErrAlreadyExists {
					return BadRequestWithError(err)
				}
				return InternalServerError(err)
			}			
		break
	}


	return RedirectToURL() //ResponseOkay(ctx,req)
}

/* DeleteHandler */
func DeleteHandler(ctx context.Context,w http.ResponseWriter, req *http.Request) Response {

	log.Printf("DeleteHandler %q\n",req.URL.Path)

	elements,err := ProcessRequestForm(ctx,req)
	if err != nil {
		return BadRequestWithError(err)
	}

	m := elements.ToStringMap()


	if _,exists := m["_aspect"]; !exists {
		return BadRequestWithError(ErrNoAspect)
	}

	aspect := m["_aspect"]

	switch aspect {
		case "object":

			if req.URL.Path == "/" {
				return BadRequest()
			}

			err = DeleteBucket(ctx,path.Dir(req.URL.Path),path.Base(req.URL.Path))	
			if err != nil {
				if err == ErrNotFound {
					return NotFound()
				}
				return InternalServerError(err)
			}
		break
		case "attribute","meta":

			id,exists := m["_id"]
			if !exists {
				return BadRequest()
			}
			
			attrname,err := strconv.ParseInt(id,10,64)
			if err != nil {
				return BadRequestWithError(err)
			}

			err = DeleteAttribute(ctx,req.URL.Path,uint64(attrname))
			if err != nil {
				if err == ErrNotFound {
					return NotFound()
				}
				return InternalServerError(err)
			}
		break
	}

	return ResponseOkay(ctx,req) 
}

/* FormElement */
type FormElement struct {
	Name string
	ContentType string
	ContentDisposition string
	Size int
	Content []byte
}

/* FormElements */
type FormElements []*FormElement

/* Find */
func (fe FormElements) Find(name string) *FormElement {

	for _,element := range fe {
		if element.Name == name {
			return element
		}
	}

	return nil
}

func (fe FormElements) ToStringMap() map[string]string {

	m := make(map[string]string,0)

	for _,element := range fe {
		m[element.Name] = string(element.Content)
	}
	return m
}

/* RequestMethod */
func RequestMethod(ctx context.Context,req *http.Request) string {

	/* if method is not GET or POST then is def. XMLHTTP request */
	method := strings.ToLower(req.Method)

	if method != "get" || method != "post" {
		return method
	}

	
	req.ParseForm()

	/* TODO: check if XMLHttp request, if not then process the form and find _method */

	return method
}

func ResponseOkay(ctx context.Context, req *http.Request) Response {

	/* TODO: check for XMLHTTP */

	return Okay()
}


/* ProcessRequestForm */
func ProcessRequestForm(ctx context.Context,req *http.Request) (FormElements,error) {

	req.ParseForm()

	elements := make([]*FormElement,0)

	if strings.Contains(req.Header.Get("Content-Type"),"application/json") {

		body,err := ioutil.ReadAll(req.Body)
		if err != nil {
			return nil,err
		}
		defer req.Body.Close()

		var m map[string]string

		if err := json.Unmarshal(body,&m); err != nil {
			return nil,err
		}

		/* translate to formelement */
		for key,value := range m {
			elements = append(elements,&FormElement{Name:key,ContentType:"text/plain",ContentDisposition:"form-data",Size: len(value), Content: []byte(value)})
		}

		return elements,nil 
	}

	if !strings.Contains(req.Header.Get("Content-Type"), "multipart/form-data") {
		
		for key,values := range req.Form {
			element := &FormElement{Name: key, Size: len(values[0]), ContentType: "form-data",
															ContentDisposition: "form-data"}

			if len(values[0]) > 0 {
				element.Content = make([]byte,len(values[0]))
				copy(element.Content, []byte(values[0]))
			}
			
			elements = append(elements,element)
		}

		return elements,nil
	}


	reader,err := req.MultipartReader()
	if err != nil {
		return nil,err
	}

	for {
		part,err := reader.NextPart()
		if err != nil {
			if err == io.EOF {
				break
			}

			return nil,err
		}

		disposition := part.Header.Get("Content-Disposition")
		mediatype,params,err := mime.ParseMediaType(disposition)
		if err != nil {
			return nil,err
		}

		if mediatype != "form-data" {
			continue
		}

		name := params["name"]

		if len(name) == 0 {
			continue
		}

		body,err := ioutil.ReadAll(part)
		if err != nil {
			return nil,err
		}

		if len(body) == 0 {
			continue
		}

		contype := part.Header.Get("Content-Type")

		element := &FormElement{Name: name, Size: len(body), ContentType: contype, 
														ContentDisposition: mediatype}
		element.Content = make([]byte,len(body))
		copy(element.Content,body)

		elements = append(elements,element)
	}

	return elements,nil
}








