package main

import (
	"context"
	"strings"
	"path"
	"time"
	"bytes"
	"crypto/rand"

	"encoding/binary"
	"encoding/json"
	"encoding/hex"

	bolt "go.etcd.io/bbolt"
)

const (
	ContentUnknown = iota
	ContentMeta          
	ContentPolicy         /* R/ABAC Policy */
  ContentAttribute

	/* different combinations can be used to denote content type such as ContentCard */
)

func autoname() string {
	b := make([]byte,8)
	rand.Read(b)
	return hex.EncodeToString(b)
}

func buckets(path string) []string {
	if path == "/" {
		return nil
	}
	if path[0] == '/' {
		path = path[1:]
	}

	return strings.Split(path,"/")
}

func object(path,d string) string {
	if path == "/" {
		return d
	}
	parts := strings.Split(path,"/")
	return parts[len(parts) - 1]
}

func itob(v uint64) []byte {
	b := make([]byte,8)
	binary.BigEndian.PutUint64(b, v)
	return b
}

func itos(v uint64) string {
	return hex.EncodeToString(itob(v))
}

func btoi(b []byte) uint64 {
	return binary.BigEndian.Uint64(b) 
}

type Object struct {
	Path string
	Name string          /* TODO: actual name should come from meta attribute (_meta.name) */

	Children []string

	Meta []*Attribute
	Groups []*AttributeGroup
}

func (b *Object) Parent() string {
	return path.Dir(b.Path)
}

func (b *Object) IsRoot() bool {
	if b.Path == "/" {
		return true
	}
	return false
}

func (b *Object) Url(in string) string {
	return path.Join(b.Path,in)
}

func NewObject(thepath,name string) *Object {
	obk := &Object{Path:thepath,Name:name}
	obk.Children = make([]string,0)
	obk.Meta = make([]*Attribute,0)
	obk.Groups = make([]*AttributeGroup,0)

	return obk
}

func TimedStat(ctx context.Context, thepath string) (time.Duration,*Object,error) {
	s0 := time.Now()
	obk,err := Stat(ctx,thepath)
	return time.Since(s0),obk,err
}


/* Stat */
func Stat(ctx context.Context, thepath string) (*Object, error) {

	db := toDatabase(ctx)
	if db == nil {
		return nil,ErrBadDatabase
	}

	name := object(thepath,":@:")
	bucket := NewObject(thepath,name)


	err := db.View(func(tx *bolt.Tx) error {
		if object(thepath,":@:") == ":@:" {
			c := tx.Cursor()

			for k,v := c.First(); k != nil; k,v = c.Next() {
				if v == nil {
					bucket.Children = append(bucket.Children,string(k))
				}
			}
			return nil
		}

		var b *bolt.Bucket

		for i,part := range buckets(thepath) {
			if b == nil && i == 0 {
				b = tx.Bucket([]byte(part))
			} else {
				b = b.Bucket([]byte(part))
			}

			if b == nil {
				return ErrNotFound
			}
		}
	

		c := b.Cursor()
		for k,v := c.First(); k != nil; k,v = c.Next() {
			if v == nil {
				bucket.Children = append(bucket.Children,string(k))	
			} else {

				/* decode as attribute, first read the the first byte and determine the type */
				mask := v[0] 

				var attr Attribute
				if err := json.Unmarshal(v[1:],&attr); err != nil {
					return err
				}

				switch mask {
					case ContentMeta:
						bucket.Meta = append(bucket.Meta,attr.CopyWithId(btoi(k)))
					break
					case ContentAttribute:

						/* if has prefix then add to group */
						var group *AttributeGroup

						for _,grp := range bucket.Groups {
							if grp.Name == attr.Prefix {
								group = grp
								break
							}
						}

						if group == nil {
							group = NewAttributeGroup(attr.Prefix)
							bucket.Groups = append(bucket.Groups,group)
						}

						group.Attributes = append(group.Attributes,attr.CopyWithId(btoi(k)))
		
					break
					case ContentPolicy:

					break
				}
			}
		}

		return nil
	})


	return bucket,err
}

func TimedCreateAttribute(ctx context.Context,thepath string,attr *Attribute) (time.Duration,error) {
	s0 := time.Now()
	err := CreateAttribute(ctx,thepath,attr)
	return time.Since(s0),err
}

func CreateAttribute(ctx context.Context,thepath string,attr *Attribute) error {

	if object(thepath,":@:") == ":@:" {
		return ErrNoRootAttributes
	}

	db := toDatabase(ctx)
	if db == nil {
		return ErrBadDatabase
	}

	err := db.Update(func(tx *bolt.Tx) error {

		var b *bolt.Bucket 

		for i,part := range buckets(thepath) {
			if b == nil && i == 0 {
				b = tx.Bucket([]byte(part))
			} else {
				b = b.Bucket([]byte(part))
			}

			if b == nil {
				return ErrNotFound
			}
		}

		if b == nil {
			return ErrNotFound
		}


		id,err := b.NextSequence()
		if err != nil {
			return err
		}

		buf := bytes.NewBuffer(nil)
		buf.WriteByte(byte(ContentAttribute))
		enc := json.NewEncoder(buf)

		if err := enc.Encode(attr); err != nil {
			return err
		}

		return b.Put(itob(id), buf.Bytes())
	})

	return err
}


/* TimedCreateMetaAttribute */
func TimedCreateMetaAttribute(ctx context.Context,thepath string,attr *Attribute) (time.Duration,error) {
	s0 := time.Now()
	err := CreateMetaAttribute(ctx,thepath,attr)
	return time.Since(s0),err
}

/* CreateMetaAttribute */
func CreateMetaAttribute(ctx context.Context,thepath string,attr *Attribute) error {

	if object(thepath,":@:") == ":@:" {
		return ErrNoRootAttributes
	}

	db := toDatabase(ctx)
	if db == nil {
		return ErrBadDatabase
	}

	err := db.Update(func(tx *bolt.Tx) error {

		var b *bolt.Bucket 

		for i,part := range buckets(thepath) {
			if b == nil && i == 0 {
				b = tx.Bucket([]byte(part))
			} else {
				b = b.Bucket([]byte(part))
			}

			if b == nil {
				return ErrNotFound
			}
		}

		if b == nil {
			return ErrNotFound
		}

		id,err := b.NextSequence()
		if err != nil {
			return err
		}

		buf := bytes.NewBuffer(nil)
		buf.WriteByte(byte(ContentMeta))
		enc := json.NewEncoder(buf)

		if err := enc.Encode(attr); err != nil {
			return err
		}

		return b.Put(itob(id), buf.Bytes())
	})

	
	return err
}




func TimedCreateBucket(ctx context.Context,thepath,bucketname string) (time.Duration,error) {
	s0 := time.Now()
	err := CreateBucket(ctx,thepath,bucketname)
	return time.Since(s0),err
}

/* CreateBucket */
func CreateBucket(ctx context.Context,thepath,bucketname string) error {

	db := toDatabase(ctx)
	if db == nil {
		return ErrBadDatabase
	}

	err := db.Update(func(tx *bolt.Tx) error {

		/* special case */
		if object(thepath,":@:") == ":@:" {
			
			_,err := tx.CreateBucket([]byte(bucketname))
			return err
		}


		var b *bolt.Bucket 

		for i,part := range buckets(thepath) {
			if b == nil && i == 0 {
				b = tx.Bucket([]byte(part))
			} else {
				b = b.Bucket([]byte(part))
			}

			if b == nil {
				return ErrNotFound
			}
		}

		if b == nil {
			return ErrNotFound
		}

		_,err := b.CreateBucket([]byte(bucketname))

		return err
	})

	return err
}

/* DeleteBucket */
func DeleteBucket(ctx context.Context,thepath,bucketname string) error {

	db := toDatabase(ctx)
	if db == nil {
		return ErrBadDatabase
	}

	err := db.Update(func(tx *bolt.Tx) error {
	
		if object(thepath,":@:") == ":@:" {
			return tx.DeleteBucket([]byte(bucketname))
		}


		var b *bolt.Bucket

		for i,part := range buckets(thepath) {
			if b == nil && i == 0 {
				b = tx.Bucket([]byte(part))
			} else {
				b = b.Bucket([]byte(part))
			}

			if b == nil {
				return ErrNotFound
			}
		}

		if b == nil {
			return ErrNotFound
		}


		return b.DeleteBucket([]byte(bucketname))
	})

	return err
}

/* DeleteObject -- checks if bucket or file */ 
func DeleteObject(ctx context.Context,thepath,obkname string) error {

	db := toDatabase(ctx)
	if db == nil {
		return ErrBadDatabase
	}

	err := db.Update(func(tx *bolt.Tx) error {

		var b *bolt.Bucket 

		for i,part := range buckets(thepath) {
			if b == nil && i == 0 {
				b = tx.Bucket([]byte(part))
			} else {
				b = b.Bucket([]byte(part))
			}

			if b == nil {
				return ErrNotFound
			}
		}

		if b == nil {
			return ErrNotFound
		}

		if b1 := b.Bucket([]byte(obkname)); b1 != nil {
			return b.DeleteBucket([]byte(obkname))
		}

	
		return b.Delete([]byte(obkname))
	})

	return err
}


func DeleteAttribute(ctx context.Context,thepath string,attrname uint64) error {

	db := toDatabase(ctx)
	if db == nil {
		return ErrBadDatabase
	}

	err := db.Update(func(tx *bolt.Tx) error {

		var b *bolt.Bucket

		for i,part := range buckets(thepath) {
			if b == nil && i == 0 {
				b = tx.Bucket([]byte(part))
			} else {
				b = b.Bucket([]byte(part))
			}

			if b == nil {
				return ErrNotFound
			}
		}

		if b == nil {
			return ErrNotFound
		}

		return b.Delete(itob(attrname))
	})

	return err
}
			
func UpdateAttribute(ctx context.Context,thepath string,attrname uint64,attr *Attribute) error {



	return nil
}

func UpdateMetaAttribute(ctx context.Context, thepath string, attrname uint64, attr *Attribute) error {


	return nil
}



			

