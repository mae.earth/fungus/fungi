/* eslint-disable */
;(function() {
"use strict"

var Unknown = {
		view: function(v) {
			return m("code.z-unknown",v.attrs.nodeName);
		}
};

var Notemplate = {
	view: function(v) {
		return m("code.z-unknown","template " + v.attrs.name + " not found");
	}
};

var Label = {
	view: function(v) {
		return m("label",{class: v.attrs.class},v.attrs.text);
	}
};

var Input = {
	view: function(v) {
		return m("input",{class: v.attrs.class, type:v.attrs.type,
										min:v.attrs.min,max:v.attrs.max,maxlength:v.attrs.maxlength,minlength:v.attrs.minlength,
									name:v.attrs.name,value:v.attrs.value,
									placeholder:v.attrs.placeholder,required:v.attrs.required});

	}
};

var Textarea = {
	view: function(v) {
		return m("textarea",{class: v.attrs.class,name:v.attrs.name,
															required:v.attrs.required,placeholder:v.attrs.placeholder},v.attrs.value);
	}
};


var Option = {
	view: function(v) {
		return m("option",{class: v.attrs.class,value:v.attrs.value,selected:v.attrs.selected},v.attrs.text);
	}
};




/* templating */


function template(id) {
	if (id.length == 0)
		return null;

	return document.body.querySelector('template[id="' + "widget:" + id + '"]');
}	

function buildFromTemplate(d,t) {
	console.log("building from template ",t,d);

	return buildChildren(d,t.content);
}

/* xhr_post */
function xhr_post(form) {

	var data = {};

	_.map(form.target.querySelectorAll('[name]'),function(e) {
		data[e.name] = e.value;
	});

	m.request({
		method: "POST",
		url: form.target.action,
		headers: { "X-Requested-With":"XMLHttpRequest"},
		data: data
	}).then(function(result) {

	}).catch(function(error) {
		console.log("error ",error);
	});
}

/* xhr_delete */
function xhr_delete(form) {

	console.log("delete form ",form);

	var data = {};

	_.map(form.target.querySelectorAll('[name]'),function(e) {
		data[e.name] = e.value;
	});

	m.request({
		method: "DELETE",
		url: form.target.action,
		headers: { "X-Requested-With":"XMLHttpRequest"},
		data: data
	}).then(function(result) {

	}).catch(function(error) {
		console.log("error ",error);
	});
}

function xhr_put(form) {

	console.log("put form",form);

	var data = {};

	_.map(form.target.querySelectorAll('[name]'),function(e) {
		data[e.name] = e.value;
	});
	
	m.request({
		method: "PUT",
		url: form.target.action,
		headers: { "X-Requested-With":"XMLHttpRequest"},
		data:data
	}).then(function(result) {

	}).catch(function(error) {
		console.log("error ",error);
	});
}



function xhr(method) {
	var r = null;
	switch (method) {
		case "put":
		r = xhr_put;
	break;
	case "get":
		r = function(form) { console.log("xhr get ",form); }
	break;
	case "delete":
		r = xhr_delete;
	break;
	default:
		r = xhr_post;
	break;					
	}
	return r;
}

function buildChildren(d,e) {
	console.log("building children ",e,d);

	var all = [];

	_.map(e.children,function(k) {

		var usedtemplate = false;
		var lk = k.nodeName.toLowerCase();

		switch (k.nodeName) {
			case "DIV":
				if (k.dataset.widget != null) {
					var t = template(k.dataset.widget);
					if (t == null) 
						all.push(m(Notemplate,{name:k.dataset.widget}));
					else 
						all.push(buildFromTemplate(k.dataset,t));

					usedtemplate = true;
				} 
			break;
			case "FORM":
				/* check is we are using a restful interface */
				if (k.dataset.xhr === "yes") {
					var r = xhr(k.dataset.xhrMethod);

					all.push(m("form",{name:k.name,id:k.id,class: classnames(k),method:"post","data-xhr":k.dataset.xhrMethod,
																		action:k.action,enctype:k.enctype,
																		onsubmit:function(form) { r(form); return false;}},buildChildren(d,k)));
				} else { 
					all.push(m("form",{class: classnames(k), method:k.method,
															action:k.action,enctype:k.enctype},buildChildren(d,k)));				
				}
				usedtemplate = true;
			break
			case "LABEL":

				all.push(m(Label,{class: classnames(k), data:k.dataset,text:k.innerText}));
			break;
			case "BUTTON":

				all.push(m("button",{class: classnames(k), type:k.type},k.innerText)); 
			break;
			case "INPUT":

				var attrs = {class: classnames(k), type:k.type,
										min:k.min,max:k.max,maxlength:k.maxlength,minlength:k.minlength,
									name:k.name,value:k.value,placeholder:k.placeholder,required:k.required};
				if (k.pattern != "")
					attrs.pattern = k.pattern;
				
			
				all.push(m(Input,attrs));
				
			break;
			case "SELECT":
				all.push(m("select",{class: classnames(k),name:k.name,
															value:k.value,required:k.required},buildChildren(k.dataset,k)));
				usedtemplate = true;
			break;
			case "OPTION":
				all.push(m(Option,{class: classnames(k), value:k.value, text:k.innerText, selected:k.selected}));
			break;
			case "TEXTAREA":
				all.push(m(Textarea,{class: classnames(k),name:k.name,
															required:k.required,placeholder:k.placeholder,value:k.value}));
			break;
			default:
				all.push(m(Unknown,{nodeName:k.nodeName}));
			break;
		}
 		
		if (k.children != null && k.children.length > 0 && !usedtemplate)
			/* build the classes from the list */
			
			all.push(m(lk,{class: classnames(k)},buildChildren(k.dataset,k)));
		
	});

	return all;
}

function classnames(e) {
	if (e == null)
		return;

	var arr = [];
	_.map(e.classList,function(l) {
		arr.push(l);
	});

	return classNames(arr);
}




function build(e) {
	console.log("building widget ",e);
	
	var aspect = e;
	if (e.nodeName === "TEMPLATE")
		aspect = e.content;

	var all = [];	

	all.push(buildChildren(null,e));	
	
	console.log(all);
	var w = {
		data:{all:all},
		view:function(v) {

			return [v.state.data.all];
		}
	};
	return w;	
}



window.z = {
	consume: function(e) {
		return build(e);
	},
	
};

}());
