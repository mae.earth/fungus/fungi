package http

import (
	"testing"
	. "github.com/smartystreets/goconvey/convey"


	"fmt"
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
)

func write(resp *http.Response) {

	body,_ := ioutil.ReadAll(resp.Body)

	fmt.Printf("\nstatus code %s\n",http.StatusText(resp.StatusCode))
	fmt.Printf("content-type %s\n",resp.Header.Get("Content-Type"))
	fmt.Printf("\n\n%s\n",string(body))	
}	



func Test_Examples(t *testing.T) {
	Convey("Examples",t,func() {

		Convey("Not Implemented",func() {

			req := httptest.NewRequest("GET","/not-implemented",nil)
			w := httptest.NewRecorder()

			Handle(nil)(w,req)

			write(w.Result())
		})

		Convey("Helo",func() {			

			req := httptest.NewRequest("GET", "/hello",nil)
			w := httptest.NewRecorder()
		
			Handle(Helo)(w,req)			

			write(w.Result())
		})

		Convey("JSON Helo",func() {

			m := make(map[string]string,0)
			m["foo"] = "bar"
			m["soo"] = "my"

			body,err := json.Marshal(m)
			So(err,ShouldBeNil)

			req := httptest.NewRequest("POST","/json-hello",bytes.NewBuffer(body))
			req.Header.Set("Content-Type","application/json")

			w := httptest.NewRecorder()

			Handle(MustBeJSON(JSONHelo))(w,req)		

			write(w.Result())

		})
	})
}
