package http

import (
	"fmt"
	"net/http"
	"context"
	"io"
	"io/ioutil"	
	"time"
	"encoding/json"
	"encoding/hex"
	"crypto/rand"
	"crypto/sha1"
)

/* Info, Error, Warning, Debug need to be apart of a seperate logging system
 * the FRAME identity gets passed in the context, this allows the events to 
 * stored and recieved into different silos and then orchestrated later as 
 * required. 
 */

/* Infof */
func Infof(ctx context.Context, format string, values ...interface{}) {
	if len(format) == 0 {
		return 
	}

	env := ToFrame(ctx)

	if format[len(format) - 1] != '\n' {
		format += string('\n')
	}
	
	fmt.Printf(fmt.Sprintf("%v INFO %s ",time.Now().UTC(), env.Identity) + format, values...)
}

/* Errorf */
func Errorf(ctx context.Context, format string, values ...interface{}) {
	if len(format) == 0 {
		return
	}

	env := ToFrame(ctx)

	if format[len(format) - 1] != '\n' {
		format += string('\n')
	}

	fmt.Printf(fmt.Sprintf("%v ERROR %s \n", time.Now().UTC(), env.Identity) + format,values...)
}

/* Warningf */
func Warningf(ctx context.Context, format string, values ...interface{}) {
	if len(format) == 0 {
		return
	}

	env := ToFrame(ctx)

	if format[len(format) - 1] != '\n' {
		format += string('\n')
	}

	fmt.Printf(fmt.Sprintf("%v WARNING %s \n", time.Now().UTC(), env.Identity) + format,values...)
}

/* Debugf */
func Debugf(ctx context.Context, format string, values ...interface{}) {
	if len(format) == 0 {
		return
	}
	
	env := ToFrame(ctx)

	if format[len(format) - 1] != '\n' {
		format += string('\n')
	}

	fmt.Printf(fmt.Sprintf("%v DEBUG %s \n", time.Now().UTC(), env.Identity) + format,values...)
}

/* Identity -- TODO: this should be a plugin system */
func Identity() string {

	b := make([]byte,64)

	rand.Read(b)
	h := sha1.New()
	h.Write(b)

	return hex.EncodeToString(h.Sum(nil))
}

/* Response */
type Response struct {
	StatusCode int
}

/* Okay */
func Okay() Response {
	return Response{StatusCode:200}
}

/* BadRequest */
func BadRequest() Response {
	return Response{StatusCode:400}
}

/* NotImplemented */
func NotImplemented(w http.ResponseWriter, req *http.Request) {
	w.WriteHeader(http.StatusNotImplemented)
	w.Header().Set("Content-Type","text/plain")
	io.WriteString(w,"Not Implemented")
}

func InternalServerError(err error,w http.ResponseWriter, req *http.Request) Response {
	w.WriteHeader(http.StatusInternalServerError)
	w.Header().Set("Content-Type","text/plain")
	io.WriteString(w,http.StatusText(500))
	io.WriteString(w,err.Error())
	return Response{StatusCode:500}
} 

/* ContextKey */
type ContextKey string

var FrameKey = ContextKey("environment")

/* ToFrame */
func ToFrame(ctx context.Context) *Frame {
	if v := ctx.Value(FrameKey); v != nil {
		vc,ok := v.(*Frame)
		if ok {
			return vc
		}
	}
	return nil
}

type Frame struct {
	Timestamp time.Time
	Colour string
	
	Identity string
}

type Func func(context.Context, http.ResponseWriter, *http.Request) Response

func Handle(f func(context.Context, http.ResponseWriter, *http.Request) Response) func(http.ResponseWriter, *http.Request) {

	if f == nil {
		return NotImplemented
	}

	return func(w http.ResponseWriter, req *http.Request) {
	
		env := &Frame{Timestamp: time.Now(), Colour: "pink", Identity: Identity()}

		ctx := context.WithValue(context.Background(), FrameKey, env)		

		resp := f(ctx,w,req)

		defer func(t time.Time) {
			Infof(ctx, "FRAME %s %d %s %s %v", req.Method, resp.StatusCode,http.StatusText(resp.StatusCode),req.URL.Path,time.Since(t))
		}(time.Now())

		if resp.StatusCode >= 400 {
			w.WriteHeader(resp.StatusCode)
			w.Header().Set("Content-Type","text/plain")
			io.WriteString(w,http.StatusText(resp.StatusCode))
		}
			
	}
}

func ContentTypeMustBe(ct string, f func(context.Context, http.ResponseWriter, *http.Request) Response) Func {

	if f == nil {
		return nil
	}

	return func(ctx context.Context, w http.ResponseWriter, req *http.Request) Response {


		if req.Header.Get("Content-Type") != ct {
			return BadRequest()
		}

		return f(ctx,w,req)		
	}
}

func MustBeJSON(f func(context.Context, http.ResponseWriter, *http.Request) Response) Func {
	return ContentTypeMustBe("application/json",f)
}


func Helo(ctx context.Context, w http.ResponseWriter, req *http.Request) Response {

	w.Header().Set("Content-Type","text/plain")
	io.WriteString(w,"HELO\n")

	return Okay()
}

func JSONHelo(ctx context.Context, w http.ResponseWriter, req *http.Request) Response {

	body,err := ioutil.ReadAll(req.Body)
	if err != nil {
		BadRequest()
	}
	defer req.Body.Close()

	var m map[string]string

	if err := json.Unmarshal(body, &m); err != nil {
		return BadRequest()
	}	
		
	w.Header().Set("Content-Type","text/plain")
	io.WriteString(w, fmt.Sprintf("%+v\n", m))

	return Okay()
}






