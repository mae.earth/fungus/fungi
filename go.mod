module mae.earth/fungus/fungi

go 1.14

require (
	github.com/GeertJohan/go.rice v1.0.0
	github.com/smartystreets/goconvey v1.6.4
	go.etcd.io/bbolt v1.3.3
)
