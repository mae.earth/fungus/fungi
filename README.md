FUNGI
========

Fungi is a software HTTP/console interface that runs ontop of a mycelium network of components. 
With user configuration (built-in) it can create workflow interfaces. For instance to create a 
job spool, fungi can be configured to create an api over queue and store components that implements 
all the functionality of a job spool. These configurations can then be stored on the mycelium 
network for other users to make use of as needed. 
