/* nl/parser_test.go
 * mae 012018
 */
package nl

import (
	"fmt"
	"testing"
	. "github.com/smartystreets/goconvey/convey"
)

func Test_BasicParser(t *testing.T) {
	Convey("BasicParser",t,func() {

		ctx := New()
		So(ctx,ShouldNotBeNil)

		tokens,err := TokeniseString(ctx,example_001)
		So(err,ShouldBeNil)
		So(tokens,ShouldNotBeEmpty)

		So(Lexer(ctx,tokens),ShouldBeNil)

		tokens,err = Compress(ctx,tokens)
		So(err,ShouldBeNil)
		So(tokens,ShouldNotBeEmpty)
		
		fmt.Printf("\n===\n%s\n===\n",PrettyPrint(tokens))		

	})
}

				
const example_001 string = `# this is a comment
job "demo 3"
## we load a module in to give us more keywords
  with module renderman
    render frames 1 to 100 of "example.rib"
      use upto 5 nodes tagged as "fast"
      except upto 3 failures
      use minsamples of 16 and maxsamples of 512
      use viewport [0,100] to [500,500]
			set $pass as "beauty" `				
