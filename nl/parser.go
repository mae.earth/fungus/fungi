/* nl/parser.go
 * mae 012018
 */
package nl

import (
	"fmt"
	"strings"
	"text/scanner"
)

func ParseString(input string) error {

	var s scanner.Scanner

	s.Init(strings.NewReader(input))
	s.Filename = "inline"

	for tok := s.Scan(); tok != scanner.EOF; tok = s.Scan() {

		fmt.Printf("%s: %s\n",s.Position,s.TokenText())
	}

	return nil
}



/* TokeniseString */
func TokeniseString(ctx *Context,input string) ([]*Token,error) {
	if ctx == nil {
		return nil,ErrBadContext
	}
	
	ctx.mux.Lock()
	defer ctx.mux.Unlock()
	
	tokens := make([]*Token,0)

	tokens = append(tokens,&Token{Text:"-",Pos:0,Tokeniser:NewlineToken})
	word := ""
	literal := false

	for i,c := range input {
		switch c {
			case '\n':
				if literal {
					word += string(c)
				} else {
				
					if len(word) > 0 {
						tokens = append(tokens,&Token{Text:word,Pos: i - len(word), Tokeniser: AlphaToken})
					}
					word = ""
					tokens = append(tokens,&Token{Text:"-",Pos:i, Tokeniser: NewlineToken})
				}
			break
			case ' ':
				if literal {
					word += string(c)
				} else {
					if len(word) > 0 {
						tokens = append(tokens, &Token{Text: word, Pos: i - len(word), Tokeniser: AlphaToken})
					}
					word = ""
					tokens = append(tokens, &Token{Text: "space", Pos: i, Tokeniser: SpaceToken})
				}
			break
			case '\t':
				if literal {
					word += string(c)
				} else {
					if len(word) > 0 {
						tokens = append(tokens, &Token{Text: word, Pos: i - len(word), Tokeniser: AlphaToken})
					}	
					word = ""
					for i := 0; i < ctx.TabSpacing; i++ {
						tokens = append(tokens,&Token{Text:"space",Pos:i, Tokeniser: SpaceToken})
					}
				}
			break
			case '#':
				if literal {
					word += string(c)
				} else {
					if len(word) > 0 {
						tokens = append(tokens, &Token{Text: word, Pos: i - len(word), Tokeniser: AlphaToken})
					}
					word = ""
					text := "comment"
					tokens = append(tokens, &Token{Text: text, Pos: i, Tokeniser: CommentToken})
				}
			break					
			default: 
				word += string(c)

				if literal && c == '"' {
					literal = false

					tokens = append(tokens, &Token{Text: word, Pos: i - len(word), Tokeniser: LiteralToken})
					word = ""
				} else if c == '"' {
					literal = true
				}

			break
		}
	}
	/* tail */
	if len(word) > 0 {
		tokens = append(tokens, &Token{Text: word, Pos: len(input) - len(word), Tokeniser: AlphaToken})
	}
	tokens = append(tokens, &Token{Text: "-", Pos: len(input), Tokeniser: NewlineToken})

	return tokens,nil
}


/* Lexer */
func Lexer(ctx *Context,tokens []*Token) error {
	if ctx == nil {
		return ErrBadContext
	}

	ctx.mux.Lock()
	defer ctx.mux.Unlock()

	verbf := func(txt string) string {
		
		txt = strings.ToLower(txt)
		lex := ""

		for _,m := range ctx.Modules {
			found := false
			for _,keyword := range m.Keywords {
				if txt == keyword {
					found = true
					break
				}
			}
			if found {
				lex = KeywordLexical
				for _,verb := range m.Verbs {
					if txt == verb {
						lex = VerbLexical
						break
					}
				}
			}
		}
		return lex
	}

	var previous *Token
	
	for _,token := range tokens {
		switch token.Tokeniser {
			case AlphaToken:
				if previous != nil && previous.Lexical == CommentLexical {
					token.Lexical = CommentLexical
				}
				
				token.Lexical = verbf(token.Text)

			break
			case LiteralToken:
				if previous != nil && previous.Lexical == CommentLexical {
					token.Lexical = CommentLexical
				}
				

			break
			case CommentToken:
				token.Lexical = CommentLexical
			break
			case SpaceToken:
				if previous != nil && previous.Lexical == CommentLexical {
					token.Lexical = CommentLexical
				}

				/* TODO: count spaces, if no preceeding text then a depth block */
				if previous != nil && (previous.Tokeniser == SpaceToken || previous.Tokeniser == NewlineToken) {
					token.Lexical = BlockLexical
				}

			break
			case NewlineToken:

			break
			default:
				return fmt.Errorf("unknown token %q, type %q", token.Text, token.Tokeniser)
			break
		}
	
		previous = token
	} 


	return nil
}

func Compress(ctx *Context,tokens []*Token) ([]*Token,error) {
	if ctx == nil {
		return nil,ErrBadContext
	}

	ctx.mux.Lock()
	defer ctx.mux.Unlock()

	out := make([]*Token,0)

	/* remove all the unimportant space tokens from the stream */

	for _,token := range tokens {

		if token.Tokeniser == SpaceToken && token.Lexical != BlockLexical {
			continue
		}

		out = append(out,token)
	}

	return out,nil
}














