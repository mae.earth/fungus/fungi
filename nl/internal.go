/* nl/internal.go
 * mae 012018
 */
package nl


import (
	"sync"
	"errors"
)

/* verb, adjective, adverb, determiner, noun phrase, clause. 
 *
 * verb :- conveys action (subject and objects)
 * adverb :- modifier of a verb, adjective etc  
 * adjective :- describing word 
 * determiners :- are words such as the, my, this, some, each, any which are used before nouns
 * noun :- a noun phrase consists of a noun or pronoun, which is called the head and any dependent words before or after the head.
 * clause :- a form of statement
 *
 * []{...} = modules:S(token)
 */




var (
	ErrBadContext error = errors.New("Bad Context")
	ErrBadStream error = errors.New("Bad Stream")
)

const (
	DefaultTabSpacing int = 2
)

const (
	AlphaToken string = "alpha"
	LiteralToken string = "literal"
	NewlineToken string = "newline"
	SpaceToken string = "space"
	CommentToken string = "comment"

	KeywordLexical string = "keyword"

	VerbLexical string = "verb"
	AdjectiveLexical string = "adjective"
	AdverbLexical string = "adverb"
	DeterminerLexical string = "determiner"
	NounLexical string = "noun"
	ClauseLexical string = "clause"

	CommentLexical string = "comment"
	TokenLexical string = "token"
	GarbageLexical string = "garbage"
	BlockLexical string = "block"
	
	NodeAnalysis string = "node"
	PortAnalysis string = "port"
	ArcAnalysis string = "arc"
	ActorAnalysis string = "actor"
	TokenAnalysis string = "token"

	EngineAnalysis string = "engine"
	FrameAnalysis string = "frame"
	GarbageAnalysis string = "garbage"
)

/* TODO: these are here just as a thing for building */

var Keywords =  []string{"job","with","to","of","use","as","set","and","or","not","upto","the"}
var Verbs = []string{"job","with","use","set"}

/* Module -- used to parse module extensions for NL */
type Module struct {
	Label string

	Keywords []string
	Verbs []string /* TODO: replace with a function based system for noticing relationships with objects */

}


/* Token */
type Token struct {
	Text string /* raw text */
 	Pos int   /* start in the text stream */

	Tokeniser string
	Lexical   string
	Analysis string
}

/* Context */
type Context struct {
	mux sync.RWMutex

	Modules []Module

	TabSpacing int /* convert tabs to spaces, TODO: move to options */
}



/* New */
func New() *Context {

	mods := make([]Module,0)
	/* add the default module */
	mods = append(mods,Module{Label:"default",Keywords:Keywords,Verbs:Verbs})

	return &Context{Modules:mods, TabSpacing: DefaultTabSpacing}
}


