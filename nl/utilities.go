package nl

import (
	"fmt"
	"strings"
)



/* PrettyPrint */
func PrettyPrint(tokens []*Token) string {

	b := new(strings.Builder)

	A := func(all ...string) string {
	
		actual := []string{}
		for _,a := range all {
			if a == "-" {
				continue
			}
			actual = append(actual,a)
		}

		return strings.Join(actual," = ") 
	}

	depth := 0

	for i,token := range tokens {

		tokeniser := "-"
		switch token.Tokeniser {
			case AlphaToken:
				tokeniser = "@"
			break
			case LiteralToken:
				tokeniser = "'"
			break
			case NewlineToken:
				tokeniser = "¶"
				depth = 0
			break
			case SpaceToken:
				tokeniser = "ð"
			break
			case CommentToken:
				tokeniser = "#"
			break
		}

		lexical := "-"
		switch token.Lexical {
			case KeywordLexical:
				lexical = "*"
			break
			case VerbLexical:
				lexical = "verb"
			break
			case CommentLexical:
				lexical = "#"
			break
			case GarbageLexical:
				lexical = "€"
			break
			case BlockLexical:
				depth++
				lexical = fmt.Sprintf("³:%d",depth)
			break
		}
		analysis := "-"
		switch token.Analysis {
			case PortAnalysis:
				analysis = "PRT"
			break
			case ArcAnalysis:
				analysis = "ARC"
			break
			case ActorAnalysis:
				analysis = "ACT"
			break
			case NodeAnalysis:
				analysis = "NDE"
			break
			case TokenAnalysis:
				analysis = "TKN"
			break
			case EngineAnalysis:
				analysis = "ENG"
			break
			case FrameAnalysis:
				analysis = "FRM"
			break
			case GarbageAnalysis:
				analysis = "human garbage"
			break
		}

		/* TODO: add other elements here */
		text := ""
		if token.Text != "-" {
			text = token.Text
		}

		b.WriteString(fmt.Sprintf("%05d\t%05d-%05d:%2d\t%20s\t%s\n",i, token.Pos, token.Pos + len(token.Text),len(token.Text), text, A(tokeniser,lexical,analysis)))
	}

	return b.String()
}	
